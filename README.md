# infiniecho

Infinite comment and Bulletin Board System practice. 

## Will be added

 - [ ] OWASP (Lucy)
 - [ ] Grid(Toast UI Grid)
 - [ ] Editor(Toast Ui Editor) 

## Development Environment
- Java 1.8 (openJDK 11) 
- STS 4.12.1 
- Spring Maven Project (MVC pattern) 
- Server Tomcat V9.0
- Mybatis, Oracle 
- Lombok
