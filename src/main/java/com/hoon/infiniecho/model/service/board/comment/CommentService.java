package com.hoon.infiniecho.model.service.board.comment;

import java.util.List;

import com.hoon.infiniecho.model.dto.board.comment.CommentDTO;

public interface CommentService {

	int addComment(CommentDTO cDTO);

	List<CommentDTO> selectCommentList();

}
