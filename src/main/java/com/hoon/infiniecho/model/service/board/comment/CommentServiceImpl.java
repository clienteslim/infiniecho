package com.hoon.infiniecho.model.service.board.comment;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hoon.infiniecho.model.dao.board.comment.CommentMapper;
import com.hoon.infiniecho.model.dto.board.comment.CommentDTO;

@Service("CommentService")
public class CommentServiceImpl implements CommentService{
	
	private final CommentMapper mapper;
	
	
	public CommentServiceImpl(CommentMapper mapper) {
		
		this.mapper = mapper;
		
	}


	@Override
	public int addComment(CommentDTO cDTO) {
		
		
		return mapper.addComment(cDTO);
	}


	@Override
	public List<CommentDTO> selectCommentList() {
		
		return mapper.selectCommentList();
	}
	
	
	
	
}
