package com.hoon.infiniecho.model.dao.board.comment;

import java.util.List;

import com.hoon.infiniecho.model.dto.board.comment.CommentDTO;

public interface CommentMapper {

	int addComment(CommentDTO cDTO);

	List<CommentDTO> selectCommentList();

	
	
}
