package com.hoon.infiniecho.model.dto.board.comment;

import java.sql.Date;

import lombok.Data;

@Data
public class CommentDTO {

	private String commentId;
	private String commentCont;
	private String commentParentId;
	private String commentLv;
	private String commentSqInGroup;
	private Date commentRegTad;
	private String commentActSt;
	
}
