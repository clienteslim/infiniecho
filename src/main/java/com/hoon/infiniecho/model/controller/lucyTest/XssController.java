package com.hoon.infiniecho.model.controller.lucyTest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.nhncorp.lucy.security.xss.XssPreventer;

@Controller
public class XssController {

	@RequestMapping(value = "/xss/useFilter", method = { RequestMethod.POST }) 
	public ModelAndView useFilter(HttpServletRequest request) throws Exception { 
		
		ModelAndView mav = new ModelAndView();
		
		String inputMsg = request.getParameter("inputMsg"); 
		String convertMsg = XssPreventer.unescape(inputMsg); 
		
		System.out.println("치환 : " + inputMsg);
		System.out.println("역치환 : " + convertMsg);
		
		/*
		 * logger.info("### Get Message(Use XSS Filter) ###");
		 * 
		 * logger.info("### 치환 => " + inputMsg);
		 * 
		 * logger.info("### 역치환 => " + convertMsg);
		 */
		
		mav.addObject("msg", inputMsg); mav.setViewName("/common/xssMessage"); 
		
		return mav;
		
		
	}

	
	@RequestMapping(value = "/xss/noneFilter", method = { RequestMethod.POST }) 
	public ModelAndView noneFilter(HttpServletRequest request) throws Exception {
		
		ModelAndView mav = new ModelAndView(); 
		
		String inputMsg = request.getParameter("inputMsg"); 
		
		
		System.out.println("치환 : " + inputMsg);		
		
		/*
		 * logger.info("### Get Message(None XSS Filter) ###");
		 * 
		 * logger.info("### => " + inputMsg); mav.addObject("msg", inputMsg);
		 */		
		mav.setViewName("/common/xssMessage"); 
		
		return mav; 
		
	}

	
}

