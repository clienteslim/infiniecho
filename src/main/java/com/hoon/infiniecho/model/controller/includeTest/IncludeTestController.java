package com.hoon.infiniecho.model.controller.includeTest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IncludeTestController {
	
	@PostMapping("/includeTest")
	public String includeTest() {
		
		return "!SUCCESS!";
	}
	
}
