package com.hoon.infiniecho.model.controller.board.comment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hoon.infiniecho.model.dto.board.comment.CommentDTO;
import com.hoon.infiniecho.model.service.board.comment.CommentService;


@RestController
@RequestMapping("/board/comment")
public class CommentController {
	
	private final CommentService cService;
	
	
	public CommentController(CommentService cService) {
		
		this.cService = cService;
	}
	
	@PostMapping("/commentList")
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	public List<Map<String,Object>> commentList(){
		
		List<CommentDTO> cList = cService.selectCommentList();
		
		List<Map<String,Object>> cListMap = new ArrayList<Map<String,Object>>();
		
		for(CommentDTO list : cList) {
			
			Map<String,Object> cMap = new HashMap<String,Object>();
						
			cMap.put("commentId" , list.getCommentId());
			cMap.put("commentCont" , list.getCommentCont());
			cMap.put("commentParentId" , list.getCommentParentId());
			cMap.put("commentLv" , list.getCommentLv());
			cMap.put("commentSqInGroup" , list.getCommentSqInGroup());
			cMap.put("commentRegTad" , list.getCommentRegTad());
			
			cListMap.add(cMap);
			
		}
		
		return cListMap;
	}
	
	
	@PostMapping("/addComment")	
	public List<CommentDTO> addComment(@RequestBody CommentDTO cDTO) {
				
		int result = cService.addComment(cDTO);
		
		List<CommentDTO> cList = cService.selectCommentList();
		
		return cList;
	}
	
	
}
