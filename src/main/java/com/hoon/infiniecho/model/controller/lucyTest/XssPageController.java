package com.hoon.infiniecho.model.controller.lucyTest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class XssPageController {
	
	@GetMapping("/xssTest")
	public String xssTest() {
		
		return "/xssTest/xss";
		
	}
	
}
