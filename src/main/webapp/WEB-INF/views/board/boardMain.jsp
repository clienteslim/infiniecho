<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>boardMain</title>

<link rel="stylesheet" href="https://uicdn.toast.com/grid/latest/tui-grid.css" />

<script src="https://uicdn.toast.com/grid/latest/tui-grid.js"></script>

<style>
	#commentArea{
		
		border: 1px solid gray;
		
	
	}
	
	#commentWriter{
		
		border: 1px solid black;
	
	}
	
	#commentInput{
		resize:none;
		overflow: hidden;		
		width: 100%;
		min-height: 50px;
	}	

</style>

</head>
<body>
	<header>	

<%@include file="../common/head.jsp" %>
<%-- <jsp:include page="../common/head.jsp"/> --%>
	</header>
	
	
	<article class="container">
		
		<section>

			<div id="grid"></div>
		
		</section>
		
		
		<section class="container">
			<div id="commentArea">
					
				<div>
					<ul>
						
						<li id="commentWriter">
							<textArea id="commentInput" onkeydown="resize(this)" onkeyup="resize(this)" type="text" placeholder="댓글을 작성해보세요."></textArea> 
							
							<button onclick="addComment();" type="button">댓글작성</button>
						</li>
			
					</ul>
				</div>	
					
			</div>
		
		</section>		

	</article>
	
	
	
	
	
	
	
	
	<script>
	
	let $cInput = document.querySelector('#commentInput');
	
	
	
	
	function resize(obj) {
		  obj.style.height = "1px";
		  obj.style.height = (12+obj.scrollHeight)+"px";
	}
	
	
	
	
	axios.post( '${path}/board/comment/commentList', {
		
		
		
	})
	.then (res=>{
		
		console.log(res.data);
		
		let $commentWriter = document.querySelector('#commentWriter');
		
		for(let i=0 ; i < res.data.length; i++){
			
			const commentLi = document.createElement('li');
			
			const rowDiv = document.createElement('div');
			rowDiv.setAttribute('class', 'row');
			
			const colDivXs2 = document.createElement('div');
			colDivXs2.setAttribute('class', 'col-xs-2');
			
			const datSpan = document.createElement('span');
			datSpan.innerHTML = moment(res.data[i].commentRegTad).format('yyyy-MM-DD hh:mm:ss');
			
			colDivXs2.append(datSpan);
			
			const colDivXs10 = document.createElement('div');
			colDivXs10.setAttribute('class', 'col-xs-10');
			
			const hr = document.createElement('hr');
			const replyBtn = document.createElement('button');
			replyBtn.setAttribute('type','button');
			replyBtn.innerHTML = '답글쓰기';
			replyBtn.setAttribute('onclick','');
			
			commentLi.innerHTML = res.data[i].commentCont;
			commentLi.setAttribute('id', res.data[i].commentId);
			commentLi.setAttribute('class', 'comments');
			
			
			colDivXs10.append(replyBtn);
			
			rowDiv.append(colDivXs2);
			rowDiv.append(colDivXs10);
			
			rowDiv.append(hr);
			commentLi.append(rowDiv);
			
			$commentWriter.before(commentLi)
			
		}
		
	})
	
	
	
	const addComment = function(){
		
		let $cInput = document.querySelector('#commentInput');
		
		let $cInputVal = document.querySelector('#commentInput').value;
		
		axios.post( '${path}/board/comment/addComment', {  
			
			commentCont	: $cInputVal 
		
		})
		
		
			.then(res=>{
				
				$cInput.value = '';
				
				$cInput.focus();
				
				let comments = document.querySelectorAll('.comments');
				
				for(let item of comments){
					
					item.remove();
				}

				let $commentWriter = document.querySelector('#commentWriter');
				
				for(let i=0 ; i < res.data.length; i++){
					
					const commentLi = document.createElement('li');
					
					const rowDiv = document.createElement('div');
					rowDiv.setAttribute('class', 'row');
					
					const colDivXs2 = document.createElement('div');
					colDivXs2.setAttribute('class', 'col-xs-2');
					
					const datSpan = document.createElement('span');
					datSpan.innerHTML = moment(res.data[i].commentRegTad).format('yyyy-MM-DD hh:mm:ss');
					
					colDivXs2.append(datSpan);
					
					const colDivXs10 = document.createElement('div');
					colDivXs10.setAttribute('class', 'col-xs-10');
					
					const hr = document.createElement('hr');
					const replyBtn = document.createElement('button');
					replyBtn.setAttribute('type','button');
					replyBtn.innerHTML = '답글쓰기';
					replyBtn.setAttribute('onclick','');
					
					commentLi.innerHTML = res.data[i].commentCont;
					commentLi.setAttribute('id', res.data[i].commentId);
					commentLi.setAttribute('class', 'comments');
					
					
					colDivXs10.append(replyBtn);
					
					rowDiv.append(colDivXs2);
					rowDiv.append(colDivXs10);
					
					rowDiv.append(hr);
					commentLi.append(rowDiv);
					
					$commentWriter.before(commentLi)
					
				}
				
				
			})
			
			
			
		};
		
	
	
	
	
	
	
	
	const Grid = tui.Grid;
	
	const instance = new Grid({
		  el: document.getElementById('grid'), // Container element
		  columns: [
		    {
		      header: 'Name',
		      name: 'name'
		    },
		    {
		      header: 'Artist',
		      name: 'artist'
		    },
		    {
		      header: 'Release',
		      name: 'release'
		    },
		    {
		      header: 'Genre',
		      name: 'genre'
		    }
		  ],
		  data: [
		    {
		      name: 'Beautiful Lies',
		      artist: 'Birdy',
		      release: '2016.03.26',
		      genre: 'Pop'
		    }
		  ]
		});
		
//		instance.resetData(newData); // Call API of instance's public method
		
/* 		var grid =  new tui.Grid({
			  data: dataSource,
			  columns: [{ title: 'Name', name: 'name' }]
			  // ...
			});
		
		
		var dataSource = {
				  withCredentials: false,  
				  initialRequest: true,
				  api: {
				      readData: { url: '/api/read', method: 'GET' },
				      createData: { url: '/api/create', method: 'POST' },
				      updateData: { url: '/api/update', method: 'PUT' },
				      deleteData: { url: '/api/delete', method: 'DELETE' },
				      modifyData: { url: '/api/modify', method: 'POST' }
				  }
				}
 */		
		
		Grid.applyTheme('striped'); // Call API of static method
		
	</script>
	
	
</body>
</html>