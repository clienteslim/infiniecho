<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html> 
<html> 
<title>XSS Filter Example</title> 

<head> 
</head> 

<body> 


<header>
<%@include file="../common/head.jsp" %>
<%-- <jsp:include page="../common/head.jsp"/> --%>  
</header>

	<form id="testForm" method="post"> 
		
		<div class="container"> 
		
		<div class="row border-bottom mb-3"> 
		<div class="col-12"> 
		<p class="h1">EX01. Lucy-xss-servlet-filter 적용</p> 
		
		</div>
		
		 </div> 
		
		<div class="row"> 
		
		<div class="col-5"> 
		
		<div class="row"> 
		
		<div class="col-2"> 
		
		<label for="inputLabel" class="visually-hidden">
		
		</label> 
		
		<input type="text" readonly class="form-control-plaintext" id="inputLabel" value="메세지"> 
		
		</div> <div class="col-auto"> 
		
		<label for="inputMsg" class="visually-hidden">
		
		</label> <input type="text" class="form-control" id="inputMsg" name="inputMsg"> 
		
		</div> <div class="col-auto">
		
		 <button type="submit" class="btn btn-primary mb-3" id="btnOn">ON</button> 
		 
		 </div> <div class="col-auto"> 
		 
		 <button type="submit" class="btn btn-primary mb-3" id="btnOff">OFF</button>
		 
		  </div> 
		  
		  </div>
		   </div>
		    </div> 
		    </div>
		  
		   </form> 



<script> $(document).ready(function() 
		
		{ $("#btnOn").click(function() {
			
			$("#testForm").prop("action", "${pageContext.servletContext.contextPath}/xss/useFilter"); 
		
			var url = $("#testForm").attr("action"); 
			var data = $("#testForm").serialize(); $.post(url, data).done(); }); 

		

			$("#btnOff").click(function() { 
				
				$("#testForm").prop("action", "${pageContext.servletContext.contextPath}/xss/noneFilter"); 
			
				var url = $("#testForm").attr("action"); 
			
				var data = $("#testForm").serialize(); $.post(url, data).done(); 
		
		}); 
		
		
		}); 
		</script> 
		  
		  </body> 
		  
		  </html>


